package switchtwentytwenty.project.datamodel.shared;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@NoArgsConstructor
public class CategoryIdJPA implements Serializable {
    private static final long serialVersionUID = 0;
    @Getter
    private int id;

    /**
     * Constructor for the Category Id JPA object.
     *
     * @param id the id of the transaction.
     */
    public CategoryIdJPA(Object id) {
        this.id = (int) id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CategoryIdJPA that = (CategoryIdJPA) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
