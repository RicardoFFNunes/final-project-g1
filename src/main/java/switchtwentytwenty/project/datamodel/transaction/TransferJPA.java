package switchtwentytwenty.project.datamodel.transaction;

import lombok.Getter;
import lombok.NoArgsConstructor;
import switchtwentytwenty.project.datamodel.shared.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@NoArgsConstructor
@Table(name = "Transfers")
public class TransferJPA extends TransactionJPA {
    @Getter
    DestinationAccountIdJPA destinationAccountIdJPA;

    /**
     * Constructor for the TransferJPA object.
     *
     * @param originAccountIdJPA      id of the cash account the transfer is transferred from.
     * @param destinationAccountIdJPA id of the cash account the transfer is transferred to.
     * @param amountJPA               amount of the transfer.
     * @param descriptionJPA          description of the transfer.
     * @param transactionDateJPA      date of the transfer.
     * @param categoryIdJPA           transfer's category id.
     */
    public TransferJPA(OriginAccountIdJPA originAccountIdJPA, DestinationAccountIdJPA destinationAccountIdJPA, AmountJPA amountJPA, DescriptionJPA descriptionJPA, TransactionDateJPA transactionDateJPA, CategoryIdJPA categoryIdJPA) {
        super(originAccountIdJPA, amountJPA, descriptionJPA, transactionDateJPA, categoryIdJPA, false);
        this.destinationAccountIdJPA = destinationAccountIdJPA;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        TransferJPA that = (TransferJPA) o;
        return Objects.equals(destinationAccountIdJPA, that.destinationAccountIdJPA);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), destinationAccountIdJPA);
    }
}