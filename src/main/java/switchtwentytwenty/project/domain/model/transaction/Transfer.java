package switchtwentytwenty.project.domain.model.transaction;

import lombok.Getter;
import switchtwentytwenty.project.domain.model.interfaces.Entity;
import switchtwentytwenty.project.domain.model.shared.*;

import java.util.Objects;

public class Transfer extends Transaction implements Entity {

    @Getter
    private final AccountId destinationAccountId;

    /**
     * Constructor for the Transfer object.
     *
     * @param builder the Transfer Builder object.
     */
    private Transfer(TransferBuilder builder) {
        this.accountId = builder.originAccountId;
        this.destinationAccountId = builder.destinationAccountId;
        this.amount = builder.amount;
        this.transactionId = builder.transactionId;
        this.description = builder.description;
        this.date = builder.date;
        this.categoryId = builder.categoryId;
    }

    /**
     * This method allows to know if has the transaction id.
     *
     * @param id id of the transaction.
     * @return true if has the transaction id and false if not.
     */
    @Override
    public boolean hasId(TransactionId id) {
        return this.transactionId == id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Transfer transfer = (Transfer) o;
        return Objects.equals(destinationAccountId, transfer.destinationAccountId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), destinationAccountId);
    }

    public static class TransferBuilder {
        private final AccountId originAccountId;
        private final AccountId destinationAccountId;
        private final TransactionAmount amount;
        private TransactionId transactionId;
        private Description description;
        private TransactionDate date;
        private CategoryId categoryId;

        /**
         * Constructor for the Transfer Builder object.
         *
         * @param originAccountId      id of the cash account the transfer is transferred from.
         * @param destinationAccountId id of the cash account the transfer is transferred to.
         * @param amount               double amount of the transfer.
         */
        public TransferBuilder(AccountId originAccountId, AccountId destinationAccountId, TransactionAmount amount) {
            this.originAccountId = originAccountId;
            this.destinationAccountId = destinationAccountId;
            this.amount = amount;
        }

        public TransferBuilder withTransactionId(TransactionId transactionId) {
            this.transactionId = transactionId;
            return this;
        }

        public TransferBuilder withTransactionId() {
            this.transactionId = new TransactionId(0);
            return this;
        }

        public TransferBuilder withDescription(Description description) {
            this.description = description;
            return this;
        }

        public TransferBuilder withTransactionDate(TransactionDate transactionDate) {
            this.date = transactionDate;
            return this;
        }

        public TransferBuilder withCategoryId(CategoryId categoryId) {
            this.categoryId = categoryId;
            return this;
        }

        public Transfer build() {
            return new Transfer(this);
        }
    }
}