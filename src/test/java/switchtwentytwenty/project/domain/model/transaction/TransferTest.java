package switchtwentytwenty.project.domain.model.transaction;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.model.shared.*;

import static org.junit.jupiter.api.Assertions.*;

class TransferTest {

    AccountId accountId;
    AccountId destinationAccountId;
    TransactionAmount amount;
    TransactionId transactionId;
    Description description;
    TransactionDate date;
    CategoryId categoryId;

    @BeforeEach
    void initialize() {
        accountId = new AccountId(1);
        destinationAccountId = new AccountId(2);
        amount = new TransactionAmount(-12.23, Currency.BRL);
        transactionId = new TransactionId(3);
        description = new Description("Phone bill");
        date = new TransactionDate("20/06/2021 20:06");
        categoryId = new CategoryId(3);
    }

    @Test
    void transfer_Successfully() {
        Transfer transfer = new Transfer.TransferBuilder(accountId, destinationAccountId, amount)
                .withDescription(description)
                .withTransactionDate(date)
                .withCategoryId(categoryId)
                .withTransactionId(transactionId)
                .build();

        assertNotNull(transfer);
    }

    @Test
    void transfer_Successfully_WithoutTransactionId() {
        Transfer transfer = new Transfer.TransferBuilder(accountId, destinationAccountId, amount)
                .withDescription(description)
                .withTransactionDate(date)
                .withCategoryId(categoryId)
                .withTransactionId()
                .build();

        assertNotNull(transfer);
    }

    @Test
    void ensurePersonAttributesAreCorrect() {
        AccountId expectedAccountId = new AccountId(1);
        AccountId notExpectedAccountId = new AccountId(2);
        AccountId expectedDestinationAccountId = new AccountId(2);
        AccountId notExpectedDestinationAccountId = new AccountId(3);
        TransactionAmount expectedAmount = new TransactionAmount(-12.23, Currency.BRL);
        TransactionAmount notExpectedAmount = new TransactionAmount(-12.24, Currency.BRL);
        TransactionId expectedTransactionId = new TransactionId(3);
        TransactionId unexpectedTransactionId = new TransactionId(4);
        Description expectedDescription = new Description("Phone bill");
        Description unexpectedDescription = new Description("Shopping");
        TransactionDate expectedDate = new TransactionDate("20/06/2021 20:06");
        TransactionDate unexpectedDate = new TransactionDate("20/06/2020 20:06");
        CategoryId expectedCategoryId = new CategoryId(3);
        CategoryId unexpectedCategoryId = new CategoryId(6);

        Transfer transfer = new Transfer.TransferBuilder(accountId, destinationAccountId, amount)
                .withDescription(description)
                .withTransactionDate(date)
                .withCategoryId(categoryId)
                .withTransactionId(transactionId)
                .build();

        AccountId resultAccountId = transfer.getAccountId();
        AccountId resultDestinationAccountId = transfer.getDestinationAccountId();
        TransactionAmount resultAmount = transfer.getAmount();
        TransactionId resultTransactionId = transfer.getTransactionId();
        Description resultDescription = transfer.getDescription();
        TransactionDate resultDate = transfer.getDate();
        CategoryId resultCategoryId = transfer.getCategoryId();

        assertEquals(expectedAccountId, resultAccountId);
        assertEquals(expectedDestinationAccountId, resultDestinationAccountId);
        assertEquals(expectedAmount, resultAmount);
        assertEquals(expectedTransactionId, resultTransactionId);
        assertEquals(expectedDescription, resultDescription);
        assertEquals(expectedDate, resultDate);
        assertEquals(expectedCategoryId, resultCategoryId);

        assertNotEquals(notExpectedAccountId, resultAccountId);
        assertNotEquals(notExpectedDestinationAccountId, resultDestinationAccountId);
        assertNotEquals(notExpectedAmount, resultAmount);
        assertNotEquals(unexpectedTransactionId, resultTransactionId);
        assertNotEquals(unexpectedDescription, resultDescription);
        assertNotEquals(unexpectedDate, resultDate);
        assertNotEquals(unexpectedCategoryId, resultCategoryId);
    }

    @Test
    void hasId() {
        TransactionId transferId = new TransactionId(1);
        Transfer transfer = new Transfer.TransferBuilder(accountId, destinationAccountId, amount)
                .withDescription(description)
                .withTransactionDate(date)
                .withCategoryId(categoryId)
                .withTransactionId(transferId)
                .build();

        boolean result = transfer.hasId(transferId);

        assertTrue(result);
    }

    @Test
    void setTransactionId() {
        TransactionId transferId = new TransactionId(1);
        Transfer transfer = new Transfer.TransferBuilder(accountId, destinationAccountId, amount)
                .withDescription(description)
                .withTransactionDate(date)
                .withCategoryId(categoryId)
                .withTransactionId()
                .build();

        transfer.setTransactionId(transferId);

        TransactionId resultId = transfer.getTransactionId();

        assertEquals(transferId, resultId);
    }

    @Test
    void testEquals() {
        AccountId accountId1 = new AccountId(3);
        AccountId destinationAccountId1 = new AccountId(4);
        TransactionAmount amount1 = new TransactionAmount(-25.78, Currency.GBP);
        Description description1 = new Description("Electricity bill");
        TransactionDate transactionDate1 = new TransactionDate("12/09/2010 14:00");
        CategoryId categoryId1 = new CategoryId(2);
        Description destinationEntity1 = new Description("EDP");
        TransactionId transactionId1 = new TransactionId(12);
        Transfer transfer1 = new Transfer.TransferBuilder(accountId1, destinationAccountId1, amount1)
                .withDescription(description1)
                .withTransactionDate(transactionDate1)
                .withCategoryId(categoryId1)
                .withTransactionId(transactionId1)
                .build();

        AccountId accountId2 = new AccountId(1);
        AccountId destinationAccountId2 = new AccountId(2);
        TransactionAmount amount2 = new TransactionAmount(-14.98, Currency.EUR);
        Description description2 = new Description("Water bill");
        TransactionDate transactionDate2 = new TransactionDate("13/08/2009 16:00");
        CategoryId categoryId2 = new CategoryId(4);
        TransactionId transactionId2 = new TransactionId(12);

        Transfer transfer2 = new Transfer.TransferBuilder(accountId2, destinationAccountId2, amount2)
                .withDescription(description2)
                .withTransactionDate(transactionDate2)
                .withCategoryId(categoryId2)
                .withTransactionId(transactionId2)
                .build();

        Transfer transferJPA1a = transfer1;
        Transfer transferJPA1b = new Transfer.TransferBuilder(accountId1, destinationAccountId1, amount1)
                .withDescription(description1)
                .withTransactionDate(transactionDate1)
                .withCategoryId(categoryId1)
                .withTransactionId(transactionId1)
                .build();

        assertEquals(transfer1.getCategoryId(), transferJPA1b.getCategoryId());
        assertEquals(transfer1.isPayment(), transferJPA1b.isPayment());
        assertEquals(transfer1.getAccountId(), transferJPA1b.getAccountId());
        assertEquals(transfer1.getAmount(), transferJPA1b.getAmount());
        assertEquals(transfer1.getDate(), transferJPA1b.getDate());
        assertEquals(transfer1.getDescription(), transferJPA1b.getDescription());
        assertEquals(transfer1.getTransactionId(), transferJPA1b.getTransactionId());
        assertEquals(transfer1, transferJPA1a);
        assertEquals(transfer1, transferJPA1b);
        assertSame(transfer1, transferJPA1a);
        assertNotSame(transfer1, transferJPA1b);
        assertEquals(transfer1.hashCode(), transferJPA1a.hashCode());
        assertNotEquals(0, transfer2.hashCode());
        assertNotEquals(0, transfer1.getTransactionId());
        assertEquals(transfer1.hashCode(), transferJPA1b.hashCode());
        assertNotEquals(transfer1, transfer2);
        assertNotEquals(transfer1.hashCode(), transfer2.hashCode());
        assertNotEquals(0, transfer1.hashCode());
        assertNotEquals(null, transfer1);
        assertFalse(transfer1.equals(null));
        assertFalse(transfer1.equals(transfer2));
        assertTrue(transfer1.equals(transferJPA1b));
    }
}