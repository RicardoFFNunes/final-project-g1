package switchtwentytwenty.project.dto.transaction;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.model.shared.Email;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TransactionListDTOTest {

    @Test
    void createTransactionListDTOSuccessfully() {
        long transactionIdOne = 123;
        double amountOne = 34.45;
        String currencyOne = "EUR";
        String descriptionOne = "Transaction";
        String dateOne = "12/02/2020 15:56";
        String categoryNameOne = "Groceries";
        TransactionOutputDTO transactionOutputDTOOne = new TransactionOutputDTO(transactionIdOne, amountOne, currencyOne, descriptionOne, dateOne, categoryNameOne);

        long transactionIdTwo = 321;
        double amountTwo = 34.46;
        String currencyTwo = "USD";
        String descriptionTwo = "Transaction Two";
        String dateTwo = "12/02/2021 15:56";
        String categoryNameTwo = "Shopping";
        TransactionOutputDTO transactionOutputDTOTwo = new TransactionOutputDTO(transactionIdTwo, amountTwo, currencyTwo, descriptionTwo, dateTwo, categoryNameTwo);
        List<TransactionOutputDTO> dtoList = new ArrayList<>();
        dtoList.add(transactionOutputDTOOne);
        dtoList.add(transactionOutputDTOTwo);

        TransactionListDTO transactionListDTO = new TransactionListDTO(dtoList);

        assertNotNull(transactionListDTO);
        assertEquals(dtoList, transactionListDTO.getTransactionList());
    }

    @Test
    void testGetterAndSetter() {
        //arrange
        List<TransactionOutputDTO> expected = new ArrayList<>();
        TransactionOutputDTO transactionOutputDTO = new TransactionOutputDTO();
        expected.add(transactionOutputDTO);

        //act
        TransactionListDTO result = new TransactionListDTO(expected);

        //assert
        assertNotNull(result);
        assertEquals(expected, result.getTransactionList());
    }

    @Test
    void testEquals_test1_DifferentObjects() {
        //arrange
        List<TransactionOutputDTO> transactionList = new ArrayList<>();
        TransactionOutputDTO expected = new TransactionOutputDTO();
        transactionList.add(expected);

        //act
        TransactionListDTO result = new TransactionListDTO(transactionList);
        TransactionListDTO otherResult = new TransactionListDTO(transactionList);

        //assert
        assertNotSame(result, otherResult);
        assertEquals(result, otherResult);
    }

    @Test
    void testEquals_test2_SameObjects() {
        //arrange
        List<TransactionOutputDTO> transactionList = new ArrayList<>();
        TransactionOutputDTO expected = new TransactionOutputDTO();
        transactionList.add(expected);

        //act
        TransactionListDTO result = new TransactionListDTO(transactionList);
        TransactionListDTO otherResult = result;

        //assert
        assertSame(result, otherResult);
        assertEquals(result, otherResult);
    }

    @Test
    void testEquals_test3_DifferentObjectsType() {
        //arrange
        List<TransactionOutputDTO> transactionList = new ArrayList<>();
        TransactionOutputDTO expected = new TransactionOutputDTO();
        transactionList.add(expected);
        Email email = new Email("admin@gmail.com");

        //act
        TransactionListDTO result = new TransactionListDTO(transactionList);

        //assert
        assertNotEquals(result, email);
    }

    @Test
    void testEquals_test4_NotEqualObjects() {
        //arrange
        List<TransactionOutputDTO> transactionList = new ArrayList<>();
        TransactionOutputDTO expected = new TransactionOutputDTO();
        transactionList.add(expected);
        List<TransactionOutputDTO> otherTransactionList = new ArrayList<>();

        //act
        TransactionListDTO result = new TransactionListDTO(transactionList);
        TransactionListDTO otherResult = new TransactionListDTO(otherTransactionList);

        //assert
        assertNotEquals(result, otherResult);
    }

    @Test
    void testHashCode() {
        //arrange
        List<TransactionOutputDTO> transactionList = new ArrayList<>();
        TransactionOutputDTO expected = new TransactionOutputDTO();
        transactionList.add(expected);

        //act
        TransactionListDTO result = new TransactionListDTO(transactionList);
        TransactionListDTO otherResult = new TransactionListDTO(transactionList);

        //assert
        assertNotSame(result, otherResult);
        assertEquals(result.hashCode(), otherResult.hashCode());
    }

    @Test
    void testHashCode_false() {
        //arrange
        List<TransactionOutputDTO> transactionList = new ArrayList<>();
        TransactionOutputDTO expected = new TransactionOutputDTO();
        transactionList.add(expected);
        List<TransactionOutputDTO> otherTransactionList = new ArrayList<>();

        //act
        TransactionListDTO result = new TransactionListDTO(transactionList);
        TransactionListDTO otherResult = new TransactionListDTO(otherTransactionList);

        //assert
        assertNotEquals(result.hashCode(), otherResult.hashCode());
    }
}